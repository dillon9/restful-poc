#!flask/bin/python
from flask import Flask, jsonify
from random import randint

app = Flask(__name__)

@app.route('/api/genString', methods=['GET'])
def genNonsense():
	string = { "title" : "Random String", "data" : "", "length" : 0}
	bar = ""
	charL = "qwertyuiopasdfghjklzxcvbnm"

	#don't bother reading this garbage
	while randint(0,8) != 0:
		while randint(0,5) != 0:
			bar += str(charL[randint(0,25)])
		bar += " "
	bar = bar[:len(bar)-1]
	bar += "."
	#just skip to here!

	if not bar:
		bar = "Please re-curl, our algorithm has failed"

	string["data"] = bar
	string["length"] = len(bar)

	return jsonify(string)

@app.route('/api/magicBall', methods=['GET'])
def ball():
	string = { "title" : "8 Ball Responses", "data" : "", "length" : 0}
	responses = ["It is certain", "Reply hazy, try again", "Don't count on it", "It is decidedly so", "Ask again later", "My reply is no", "Without a doubt", "Better not tell you now", "My sources say no", "Yes definitely", "Cannot predict now", "Outlook not so good", "You may rely on it", "Concentrate and ask again", "Very doubtful", "As I see it, yes", "Most likely", "Outlook good", "Yes", "Signs point to yes"]

	string["data"] = responses[randint(0,19)]
	string["length"] = len(string["data"])

	return jsonify(string)


if __name__ == '__main__':
    app.run(debug=True)